//
//  BookingRequestController.m
//  PONLE CLIENT
//
//  Created by My Mac on 10/2/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "BookingRequestController.h"

@interface BookingRequestController ()

@end

@implementation BookingRequestController
{
    NSString *strForUserId,*strForUserToken,*strSourceLat,*strSourceLong,*strForRequestID,*strForDestiLatitude,*strForDestiLongitude,*strForTypeid,*strForCurLatitude,*strForCurLongitude;
    
    BOOL is_paymetCard,is_Fare;
    
    NSDictionary* aPlacemark;
    NSMutableArray *placeMarkArrSource,*placeMarkArrDestination,*arrForAddress;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setBackBarItem];
    self.btnNavigation.titleLabel.font=[UberStyleGuide fontRegular];
    self.tableForSource.hidden=YES;
    self.tableForDestination.hidden=YES;
    self.viewForDuration.hidden=YES;
    self.viewForOutOfTown.hidden=YES;
    arrForAddress = [[NSMutableArray alloc]init];
    placeMarkArrSource = [[NSMutableArray alloc]init];
    placeMarkArrDestination = [[NSMutableArray alloc]init];

    [self.txtSourceAddress setValue:[UIColor darkGrayColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtDestinationAddress setValue:[UIColor darkGrayColor]
                         forKeyPath:@"_placeholderLabel.textColor"];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 
#pragma mark - Action Methods

- (IBAction)onClickTimeHire:(id)sender
{
    if([self.txtSourceAddress.text isEqual:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enter Pick Up Location" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([self.txtDestinationAddress.text isEqual:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enter Drop up Location" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else
    {
        self.viewForDuration.hidden=NO;
        self.btnTimeHire.hidden=YES;
        self.btnOutOfTown.hidden=YES;
    }
}

- (IBAction)onClickOutOfTown:(id)sender
{
    if([self.txtSourceAddress.text isEqual:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enter Pick Up Location" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([self.txtDestinationAddress.text isEqual:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enter Drop up Location" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else
    {
        self.viewForOutOfTown.hidden=NO;
        self.btnTimeHire.hidden=YES;
        self.btnOutOfTown.hidden=YES;
    }

}

- (IBAction)onClickConfirmBooking:(id)sender
{
    if(self.txtEnterDuration.text < [NSString stringWithFormat:@"%d",4] || self.txtEnterDuration.text > [NSString stringWithFormat:@"%d",9])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"You should enter duration between 4 to 9 only" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [self TimeHireBooking];
    }
}

- (IBAction)onClickCancelBooking:(id)sender
{
    self.viewForDuration.hidden=YES;
    self.btnOutOfTown.hidden=NO;
    self.btnTimeHire.hidden=NO;
}

- (IBAction)onClickOutOfTownConfirm:(id)sender
{
    [self OutOfTownBooking];
}

- (IBAction)onClickOutOfTownCancel:(id)sender
{
    
}

#pragma mark- Searching Method

- (IBAction)SourceLocation:(id)sender
{
    aPlacemark=nil;
    [placeMarkArrSource removeAllObjects];
    self.tableForSource.hidden=YES;
    //  CLGeocoder *geocoder;
    
    NSString *str=self.txtSourceAddress.text;
    NSLog(@"%@",str);
    if(str == nil)
        self.tableForSource.hidden=YES;
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    //[dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:str forKey:@"input"]; // AUTOCOMPLETE API
    [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             //NSArray *arrAddress=[response valueForKey:@"results"];
             NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
             
             NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
             
             if ([arrAddress count] > 0)
             {
                 self.tableForSource.hidden=NO;
                 
                 placeMarkArrSource=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                 //[placeMarkArr addObject:Placemark]; o
                 [self.tableForSource reloadData];
                 
                 if(arrAddress.count==0)
                 {
                     self.tableForSource.hidden=YES;
                 }
             }
             
         }
         
     }];
}

- (IBAction)DestinationLocation:(id)sender
{
    aPlacemark=nil;
    [placeMarkArrSource removeAllObjects];
    self.tableForDestination.hidden=YES;
    //  CLGeocoder *geocoder;
    
    NSString *str=self.txtDestinationAddress.text;
    NSLog(@"%@",str);
    if(str == nil)
        self.tableForDestination.hidden=YES;
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    //[dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:str forKey:@"input"]; // AUTOCOMPLETE API
    [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             //NSArray *arrAddress=[response valueForKey:@"results"];
             NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
             
             NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
             
             if ([arrAddress count] > 0)
             {
                 self.tableForDestination.hidden=NO;
                 
                 placeMarkArrDestination=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                 //[placeMarkArr addObject:Placemark];
                 [self.tableForDestination reloadData];
                 
                 if(arrAddress.count==0)
                 {
                     self.tableForDestination.hidden=YES;
                 }
             }
             
         }
         
     }];
}

#pragma mark - 
#pragma mark - self methods


-(void)TimeHireBooking
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:strForUserId forKey:PARAM_ID];
        [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
        [dictParam setValue:strSourceLat forKey:PARAM_LATITUDE];
        [dictParam setValue:strSourceLong  forKey:PARAM_LONGITUDE];
        [dictParam setValue:strForDestiLatitude forKey:PARAM_DEST_LATITUDE];
        [dictParam setValue:strForDestiLongitude forKey:PARAM_DEST_LONGITUDE];
        [dictParam setValue:@"" forKey:PARAM_DURATION];
        
        //[dictParam setValue:strForTypeid forKey:PARAM_TYPE];
        
        if (is_paymetCard)
        {
            [dictParam setValue:@"0" forKey:PARAM_PAYMENT_OPT];
        }
        else
        {
            [dictParam setValue:@"1" forKey:PARAM_PAYMENT_OPT];
        }
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_REQUEST_FOR_DURATION withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSLog(@"pick up......%@",response);
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         NSMutableDictionary *walker=[response valueForKey:@"walker"];
                         //[self showDriver:walker];
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         
                         strForRequestID=[response valueForKey:@"request_id"];
                         [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                         
                     }
                 }
                 else
                 {
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                     
                 }
             }
             
         }];
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }

}


-(void)OutOfTownBooking
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setValue:strForUserId forKey:PARAM_ID];
        [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
        [dictParam setValue:strSourceLat forKey:PARAM_LATITUDE];
        [dictParam setValue:strSourceLong  forKey:PARAM_LONGITUDE];
        [dictParam setValue:strForDestiLatitude forKey:PARAM_DEST_LATITUDE];
        [dictParam setValue:strForDestiLongitude forKey:PARAM_DEST_LONGITUDE];
        [dictParam setValue:@"" forKey:PARAM_DATETIME];
        
        //[dictParam setValue:strForTypeid forKey:PARAM_TYPE];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_REQUEST_FOR_OUT_OF_TOWN withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSLog(@"pick up......%@",response);
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         NSMutableDictionary *walker=[response valueForKey:@"walker"];
                         //[self showDriver:walker];
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         
                         strForRequestID=[response valueForKey:@"request_id"];
                         [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                         
                     }
                 }
                 else
                 {
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                     
                 }
             }
             
         }];
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    
}

#pragma mark - Tableview Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(cell == nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    if(tableView==self.tableForSource)
    {
        NSString *formatedAddress=[[placeMarkArrSource objectAtIndex:indexPath.row] valueForKey:@"description"];
        cell.textLabel.text=formatedAddress;
    }
    else
    {
        NSString *formatedAddress=[[placeMarkArrDestination objectAtIndex:indexPath.row] valueForKey:@"description"];
        cell.textLabel.text=formatedAddress;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self.tableForSource)
    {
        aPlacemark=[placeMarkArrSource objectAtIndex:indexPath.row];
        self.tableForSource.hidden=YES;
        [self setNewPlaceDataSource];
    }
    else
    {
        aPlacemark=[placeMarkArrDestination objectAtIndex:indexPath.row];
        self.tableForDestination.hidden=YES;
        [self setNewPlaceDataDestination];
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==self.tableForSource)
        return placeMarkArrSource.count;
    else
        return placeMarkArrDestination.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)setNewPlaceDataSource
{
    self.txtSourceAddress.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
    [self textFieldShouldReturn:self.txtSourceAddress];
}

-(void)setNewPlaceDataDestination
{
    self.txtDestinationAddress.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
    [self textFieldShouldReturn:self.txtDestinationAddress];
}

#pragma mark
#pragma mark - UITextfield Delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSString *strFullText=[NSString stringWithFormat:@"%@%@",textField.text,string];
    if(self.txtSourceAddress==textField)
    {
        if(arrForAddress.count==1)
            self.tableForSource.frame=CGRectMake(self.tableForSource.frame.origin.x,86+134, self.tableForSource.frame.size.width, 44);
        else if(arrForAddress.count==2)
            self.tableForSource.frame=CGRectMake(self.tableForSource.frame.origin.x, 86+78, self.tableForSource.frame.size.width, 88);
        else if(arrForAddress.count==3)
            self.tableForSource.frame=CGRectMake(self.tableForSource.frame.origin.x, 86+34, self.tableForSource.frame.size.width, 132);
        else if(arrForAddress.count==0)
            self.tableForSource.hidden=YES;
        
        [self.tableForSource reloadData];
        
    }
    else
    {
        if(arrForAddress.count==1)
            self.tableForDestination.frame=CGRectMake(self.tableForDestination.frame.origin.x,86+134, self.tableForDestination.frame.size.width, 44);
        else if(arrForAddress.count==2)
            self.tableForDestination.frame=CGRectMake(self.tableForDestination.frame.origin.x, 86+78, self.tableForDestination.frame.size.width, 88);
        else if(arrForAddress.count==3)
            self.tableForDestination.frame=CGRectMake(self.tableForDestination.frame.origin.x, 86+34, self.tableForDestination.frame.size.width, 132);
        else if(arrForAddress.count==0)
            self.tableForDestination.hidden=YES;
        
        [self.tableForDestination reloadData];
    }
    
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==self.txtSourceAddress)
    {
        self.txtSourceAddress.text=@"";
    }
    if(textField==self.txtDestinationAddress)
    {
        self.txtDestinationAddress.text=@"";
    }
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField==self.txtSourceAddress)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"source"];
        [self getLocationFromString:self.txtSourceAddress.text];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"source"];
        [self getLocationFromString:self.txtDestinationAddress.text];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==self.txtSourceAddress)
    {
        self.tableForSource.hidden=YES;
    }
    if(textField==self.txtDestinationAddress)
    {
        self.tableForDestination.hidden=YES;
    }
    
    [textField resignFirstResponder];
    return YES;
}

-(void)getLocationFromString:(NSString *)str
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             NSArray *arrAddress=[response valueForKey:@"results"];
             if ([arrAddress count] > 0)
             {
                 if([[NSUserDefaults standardUserDefaults] boolForKey:@"source"]==YES)
                    self.txtSourceAddress.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                 else
                     self.txtDestinationAddress.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                 
                 NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                 
                 if([[NSUserDefaults standardUserDefaults] boolForKey:@"source"]==YES)
                 {
                     strSourceLat=[dictLocation valueForKey:@"lat"];
                     strSourceLong=[dictLocation valueForKey:@"lng"];
                 }
                 else
                 {
                     strForDestiLatitude =[dictLocation valueForKey:@"lat"];
                     strForDestiLongitude=[dictLocation valueForKey:@"lng"];
                 }
                 /*[self getETA:[arrDriver objectAtIndex:0]];
                 CLLocationCoordinate2D coor;
                 coor.latitude=[strForLatitude doubleValue];
                 coor.longitude=[strForLongitude doubleValue];
                 
                 
                 GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:14];
                 [mapView_ animateWithCameraUpdate:updatedCamera];*/
                 // [self getProviders];
                 
             }
             
         }
         
     }];
}

@end
