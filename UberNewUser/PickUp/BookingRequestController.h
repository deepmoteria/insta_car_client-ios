//
//  BookingRequestController.h
//  PONLE CLIENT
//
//  Created by My Mac on 10/2/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "BaseVC.h"

@interface BookingRequestController : BaseVC<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;

@property (weak, nonatomic) IBOutlet UIButton *btnTimeHire;
@property (weak, nonatomic) IBOutlet UIButton *btnOutOfTown;

@property (nonatomic,strong) NSString *strForBookLatitude;
@property (nonatomic,strong) NSString *strForBookLongitude;
@property (weak, nonatomic) IBOutlet UITableView *tableForSource;
@property (weak, nonatomic) IBOutlet UITableView *tableForDestination;

@property (weak, nonatomic) IBOutlet UITextField *txtSourceAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtDestinationAddress;
@property (weak, nonatomic) IBOutlet UIView *viewForDuration;
@property (weak, nonatomic) IBOutlet UIView *viewForOutOfTown;

@property (weak, nonatomic) IBOutlet UILabel *lblEnterDuration;
@property (weak, nonatomic) IBOutlet UITextField *txtEnterDuration;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirmBooking;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelBooking;
@property (weak, nonatomic) IBOutlet UIButton *btnOutOfTownConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnOutOfTownCancel;





- (IBAction)onClickTimeHire:(id)sender;

- (IBAction)onClickOutOfTown:(id)sender;
- (IBAction)SourceLocation:(id)sender;
- (IBAction)DestinationLocation:(id)sender;
- (IBAction)onClickConfirmBooking:(id)sender;
- (IBAction)onClickCancelBooking:(id)sender;
- (IBAction)onClickOutOfTownConfirm:(id)sender;
- (IBAction)onClickOutOfTownCancel:(id)sender;




@end
